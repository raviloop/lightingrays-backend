<?php namespace App\Http\Controllers;
use DB;
use App\countries;
use App\institutes;
use App\students;
use App\sessions;
use App\images;
use App\languages_provided;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class InstitutesController extends Controller {

    public function searchInstitutes(Request $request){
        $languageId = $request['languageId'];
        $countryId = $request['countryId'];

        $instituteIds = languages_provided::where('language_id',$languageId)->pluck('institute_id');

        $institutes = institutes::where('country_id',$countryId)->whereIn('id',$instituteIds)->get();

        foreach ($institutes as $institute) {
            
            $institute['image'] = images::select('url')->where('id',$institute->image_id)->first();
            $institute['country'] = countries::select('name')->where('id',$institute->country_id)->first();
        }


        return response()->json($institutes);
        
    }
}
