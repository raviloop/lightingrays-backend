<?php 
namespace App\Http\Controllers;
use DB;
use App\countries;
use App\institutes;
use App\students;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class RegistrationsController extends Controller {

    public function getAllCountries(){
        $countries = countries::all();
        return $countries;
    }

    public function registerInstitute(Request $request){

        
        $record = institutes::where('email',$request['instituteEmail'])->get();

        if(count($record) == 0){

            $institute = new institutes;
            $institute->name = $request['instituteName'];
            $institute->country_id = $request['selectedCountry'];
            $institute->email = $request['instituteEmail'];
            $institute->password = Hash::make($request['confirmPassword']);
            $institute->address = $request['address'];
            $institute->phone_number = $request['number'];
            $institute->image_id = $request['image'];
            $institute->save();
    
            return $institute->id;
        }
        else{
            return "present";
        }


    }
    

    public function registerStudent(Request $request){

$record = students::where('email',$request['studentEmail'])->get();

if(count($record) == 0){

        $student = new students;
        
        $student->first_name = $request['firstName'];
        $student->last_name = $request['lastName'];
        $student->country_id = $request['selectedCountry'];
        $student->email = $request['studentEmail'];
        $student->password = Hash::make($request['confirmPassword']);
        $student->address = $request['address'];
        $student->phone_number = $request['number'];
        $student->image_id = $request['image'];
        $student->save();
        return $student->id;
            }
    else {
            return "present";
        }
    }

}
