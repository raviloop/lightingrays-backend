<?php namespace App\Http\Controllers;
use DB;
use App\countries;
use App\images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImagesController extends Controller {


	public function uploadImage(Request $request)
    {
        $INSTITUTE ='institute';
        $STUDENT ='student';
        $whichTable =  $request['name'];
        if($whichTable == $INSTITUTE){
            $uploadpath = 'assets/images/institute';
            $filename = 'ins'.rand().'.jpg';

        }
        else if($whichTable == $STUDENT){
            $uploadpath = 'assets/images/student';
            
            $filename = 'stu'.rand().'.png';
        }
        else{
            $uploadpath = 'assets/images';
            $filename = rand().'.png';

        }
     $file = Input::file('file');
     $file->move($uploadpath,$filename);
     chmod($uploadpath.'/'.$filename, 0777);
     echo $uploadpath.'/'.$filename;
    }


    public function saveImage(Request $request){
        $imagePath =  $request['path'];
        $images = new images;
        $images->url  = $imagePath;
        $images->save();
        return $images->id;
    }

}
