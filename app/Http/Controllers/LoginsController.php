<?php namespace App\Http\Controllers;
use DB;
use App\countries;
use App\institutes;
use App\students;
use App\sessions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LoginsController extends Controller {

public function loginCheck(Request $request){

    $email = $request['email'];
    $type = $request['type'];
    $password = $request['password'];
    $token = rand().Hash::make($email);


    if($type == 'institute'){
        $user = institutes::where('email',$email)->first();
        $user_type_id = 1;
    }
    else if($type == 'student'){
        $user =students::where('email',$email)->first();
        $user_type_id = 2;
    }
    else{
        return response()->json('Invalid Type');
    }    
        
        if($user){
            if(Hash::check($password,$user->password)){
                
                $matchThese = ['user_Id'=>$user->id,'user_type_id'=>$user_type_id];
                sessions::where($matchThese)->update([
                    'status'=> false
                ]);
                $session = new sessions;
                $session->user_type_id = $user_type_id;
                $session->token = $token;
                $session->status = true;
                $session->user_id = $user->id;
                $session->save();

                return response()->json([
                    'userId' => $user->id,
                    'user_type_id'=>$user_type_id,
                    'token'=>$token
                ]);
            }
            else{
        return response()->json('Invalid Password');
            }
        }
        else{
        return response()->json('Invalid Email');
        }
    

}

public function getUserById(Request $request){
$userId = $request['userId'];
$userType = $request['user_type_id'];
$token = $request['token'];

$matchThese = ['status'=>true,'token'=>$token,'user_Id'=>$userId,'user_type_id'=>$userType];
// return sessions::select('token')->where($matchThese)->first();
    if($token == sessions::select('token')->where($matchThese)->first()->token){
            if($userType == 1){
                $user = institutes::where('id',$userId)->first();
            }
            else if($userType == 2){
                $user = students::where('id',$userId)->first();
            }
            return response()->json($user);
            
    }
    else{
        return response()->json("Unauthorized Access");
    }

}

}
