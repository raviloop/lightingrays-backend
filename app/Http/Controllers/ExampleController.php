<?php

namespace App\Http\Controllers;
use DB;
use App\countries;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function test(){
                $users = DB::select('select * from countries');
        return $users;
    }

    public function getAllCountries(){
        $countries = countries::all();
        return $countries;
    }


	public function uploadImage(Request $request)
    {

     $uploadpath = 'assets/images';
     $file = Input::file('file');
     $filename = rand().'.png';
     $file->move($uploadpath,$filename);
     chmod('assets/images/'.$filename, 0777);
     echo $filename;
    }
    //
}
